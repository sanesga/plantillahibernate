/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Scanner;
import model.Profesor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner (System.in);
      
        int opcion;
        Profesor p = null;
        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory(); 
    
        //CREAR UNA SESION
        Session session=factory.openSession();
        
        do {
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR SEGURO
                     p=new Profesor(7,"Pepe","Garcia","Perez");
                    System.out.println("Profesor creado con éxito");
                    break;
                case 2://GUARDAR SEGURO
                    session.beginTransaction();
                    session.save(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor guardado con éxito");
                    break;
                case 3://LEER SEGURO
                    p = session.get(Profesor.class, 1);
                    System.out.println(p);
                    break;
                case 4://ACTUALIZAR
                    session.beginTransaction();
                    p.setApe1("Rodríguez");
                    session.update(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor actualizado con éxito");
                    break;
                case 5://ELIMINAR SEGURO
                    session.beginTransaction();
                    session.delete(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor eliminado con éxito");
                    break;
                case 6://SALIR
                    //cerrar la conexion
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 6);
  
    }
       public static void menu() {
        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR PROFESOR, COMO SON FIJOS DARÁ ERROR");
        System.out.println("1. Crear profesor");
        System.out.println("2. Guardar profesor");
        System.out.println("3. Leer profesor");
        System.out.println("4. Actualizar profesor");
        System.out.println("5. Eliminar profesor");
        System.out.println("6. Salir");
    }
}